﻿# Like normal landholders, but they dislike other races
ideology_greentide_adventurers = {	
	lawgroup_racial_tolerance = {
		law_same_race_only = approve
		law_giantkin_group_only = disapprove
		law_goblinoid_group_only = disapprove
		law_monstrous_only = strongly_disapprove
		law_non_monstrous_only = strongly_approve
		law_all_races_allowed = strongly_disapprove
	}
	
	lawgroup_army_model = {
		law_peasant_levies = disapprove
		law_professional_army = approve
		law_mass_conscription = neutral
		law_national_militia = approve
	}
	
	# Adventurers dislike serfdom. In general they should also like homesteading but these guys don't as they're escanni adventurers turned noble
	lawgroup_land_reform = {
		law_serfdom = neutral
		law_tenant_farmers = approve
		law_commercialized_agriculture = disapprove
		law_homesteading = disapprove
		law_collectivized_agriculture = disapprove
	}
	
	lawgroup_taxation = {
		law_consumption_based_taxation = approve
		law_land_based_taxation = approve
		law_per_capita_based_taxation = neutral
		law_proportional_taxation = disapprove
		law_graduated_taxation = strongly_disapprove
	}

	lawgroup_welfare = {
		law_no_social_security = approve
		law_poor_laws = neutral
		law_wage_subsidies = disapprove
		law_old_age_pension = disapprove
	}

	lawgroup_colonization = {
		law_colonial_resettlement = approve
		law_colonial_exploitation = approve
		law_frontier_colonization = approve
		law_no_colonial_affairs = disapprove
	}
}

ideology_baronites_paternalistic = {
	icon = "gfx/interface/icons/ideology_icons/junker_paternalistic.dds"
	
	lawgroup_governance_principles = {
		law_monarchy = strongly_approve
		law_theocracy = approve			
		law_presidential_republic = neutral
		law_parliamentary_republic = neutral
		law_council_republic = strongly_disapprove

		#Anbennar
		law_magocracy = approve
		law_stratocracy = neutral
	}
	
	lawgroup_distribution_of_power = {
		law_single_party_state = neutral
		law_landed_voting = strongly_approve
		law_autocracy = disapprove
		law_oligarchy = approve
		law_wealth_voting = approve
		law_census_voting = disapprove
		law_universal_suffrage = strongly_disapprove
		law_anarchy = strongly_disapprove
		law_technocracy = strongly_disapprove
	}
	
	lawgroup_bureaucracy = {
		law_hereditary_bureaucrats = approve
		law_appointed_bureaucrats = disapprove
		law_elected_bureaucrats = approve
	}

	lawgroup_policing = {
		law_local_police = approve
		law_dedicated_police = approve
		law_militarized_police = strongly_disapprove
		law_no_police = disapprove
	}
}

#Likes free artifice, dislikes magic, generally ok with some evilness
ideology_artifice_supremacist = {
	lawgroup_artificer_ethics = {
		law_amoral_artifice_banned = approve
		law_pragmatic_artifice = approve
		law_amoral_artifice_embraced = neutral
	}
	
	lawgroup_magic_and_artifice = {
		law_traditional_magic_only = strongly_approve
		law_traditional_magic_encouraged = approve
		law_artifice_encouraged = neutral
		law_artifice_only = strongly_disapprove
		law_mundane_production = strongly_disapprove
	}
}

#Likes Magic
ideology_magical_supremacist = {
	lawgroup_magic_and_artifice = {
		law_traditional_magic_only = strongly_approve
		law_traditional_magic_encouraged = approve
		law_artifice_encouraged = disapprove
		law_artifice_only = strongly_disapprove
		law_mundane_production = strongly_disapprove
	}
}

#Wants welfare for its people only and clear cannorian/ruinborn lines (for Aelantir settlers)
ideology_nativist = {
	icon = "gfx/interface/icons/ideology_icons/isolationist.dds"
	
	lawgroup_citizenship = {
		law_ethnostate = approve
		law_national_supremacy = strongly_approve
		law_racial_segregation = approve
		law_cultural_exclusion = neutral
		law_multicultural = disapprove
	}

	lawgroup_migration = {
		law_closed_borders = approve
		law_migration_controls = strongly_approve
		law_no_migration_controls = disapprove
	}

	lawgroup_racial_tolerance = {
		law_same_race_only = strongly_approve
		law_non_monstrous_only = disapprove
		law_all_races_allowed = strongly_disapprove
	}

	lawgroup_welfare = {
		law_no_social_security = neutral
		law_poor_laws = approve
		law_wage_subsidies = strongly_approve
		law_old_age_pension = approve
	}
}

ideology_equal = {
	icon = "gfx/interface/icons/ideology_icons/isolationist.dds"
	
	lawgroup_rights_of_women = {
		law_womens_suffrage = approve
		law_women_in_the_workplace = neutral
		law_women_own_property = disapprove
		law_no_womens_rights = strongly_disapprove
	}

	lawgroup_free_speech = {
		law_outlawed_dissent = approve
		law_censorship = approve
		law_right_of_assembly = neutral
		law_protected_speech = disapprove
	}
}

ideology_matriarchal = {
	icon = "gfx/interface/icons/ideology_icons/patriarchal.dds"
	
	lawgroup_rights_of_women = {
		law_no_womens_rights = approve
		law_women_own_property = disapprove
		law_women_in_the_workplace = disapprove
		law_womens_suffrage = strongly_disapprove
	}

	lawgroup_free_speech = {
		law_outlawed_dissent = approve
		law_censorship = approve
		law_right_of_assembly = neutral
		law_protected_speech = disapprove
	}
}


ideology_machine_servitude = {
	icon = "gfx/interface/icons/ideology_icons/plutocratic.dds"
	lawgroup_mechanim_rights = {
		law_mechanim_freedom = strongly_disapprove
		law_mechanim_banned = strongly_disapprove
		law_sapience_mechanim_compromise_enforced = approve
		law_sapience_mechanim_compromise = approve
		law_sapience_mechanim_unrecognized_enforced = strongly_approve
		law_sapience_mechanim_unrecognized = strongly_approve
	}
}

ideology_luddite_mechanim = {
	icon = "gfx/interface/icons/ideology_icons/ideology_leader/luddite.dds"
	lawgroup_mechanim_rights = {
		law_mechanim_freedom = strongly_disapprove
		law_mechanim_banned = approve
		law_sapience_mechanim_compromise_enforced = neutral
		law_sapience_mechanim_compromise = neutral
		law_sapience_mechanim_unrecognized_enforced = disapprove
		law_sapience_mechanim_unrecognized = disapprove
	}

}

ideology_anti_mechanim_trade = {
	icon = "gfx/interface/icons/ideology_icons/abolitionist.dds"
	lawgroup_mechanim_rights = {
		law_mechanim_freedom = approve
		law_mechanim_banned = neutral
		law_sapience_mechanim_compromise_enforced = disapprove
		law_sapience_mechanim_compromise = disapprove
		law_sapience_mechanim_unrecognized_enforced = strongly_disapprove
		law_sapience_mechanim_unrecognized = strongly_disapprove
	}
}

ideology_pb_machine_servitude = {
	icon = "gfx/interface/icons/ideology_icons/plutocratic.dds"
	lawgroup_mechanim_rights = {
		law_mechanim_freedom = neutral
		law_mechanim_banned = disapprove
		law_sapience_mechanim_compromise_enforced = approve
		law_sapience_mechanim_compromise = approve
		law_sapience_mechanim_unrecognized_enforced = neutral
		law_sapience_mechanim_unrecognized = neutral
	}
}