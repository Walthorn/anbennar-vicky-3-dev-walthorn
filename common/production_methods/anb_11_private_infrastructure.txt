﻿## Railways
pm_sparkdrive_locomotives = {
	texture = "gfx/interface/icons/production_method_icons/experimental_trains.dds"
	unlocking_technologies = {
		railways			
	}
	building_modifiers = {
		workforce_scaled = {
			goods_input_engines_add = 5 
			goods_input_damestear_add = 1
			goods_output_transportation_add = 20 
		}
		level_scaled = {
			building_employment_laborers_add = 3000
			building_employment_machinists_add = 1000
			building_employment_clerks_add = 750
		}
	}

	state_modifiers = {
		workforce_scaled = {
			state_infrastructure_add = 20
			state_pollution_generation_add = 25
		}
	}
}

pm_infernal_trains = {
	texture = "gfx/interface/icons/production_method_icons/trains_diesel.dds"	
	unlocking_technologies = {
		compression_ignition
	}

	building_modifiers = {
		workforce_scaled = {
			goods_input_engines_add = 5 
			goods_input_spirit_energy_add = 20
			
			goods_output_transportation_add = 50 
		}
		level_scaled = {
			building_employment_laborers_add = 1000
			building_employment_machinists_add = 2500
			building_employment_clerks_add = 1000
		}
	}

	state_modifiers = {
		workforce_scaled = {
			state_infrastructure_add = 50
			state_pollution_generation_add = 15
		}
	}
}

pm_train_cars_of_holding = {
	texture = "gfx/interface/icons/production_method_icons/passenger_carriages.dds"

	unlocking_technologies = {
		multimaterial_imbuement
	}

	building_modifiers = {
		workforce_scaled = {
			goods_input_steel_add = 5
			goods_input_artificery_doodads_add = 2

			goods_output_transportation_add = 15
		}
		level_scaled = {
			building_employment_clerks_add = 500
			building_employment_machinists_add = 250
		}
	}	
	
	state_modifiers = {
		workforce_scaled = {
			state_infrastructure_add = 10
			state_pollution_generation_add = 10
		}
	}
}

pm_anti_friction_clamps = {
	texture = "gfx/interface/icons/production_method_icons/passenger_carriages.dds"

	unlocking_technologies = {
		anti_friction_magnemancy
	}

	building_modifiers = {
		workforce_scaled = {
			goods_input_steel_add = 5
			goods_input_artificery_doodads_add = 5
			goods_input_electricity_add = 3

			goods_output_transportation_add = 30
		}
		level_scaled = {
			building_employment_clerks_add = 500
			building_employment_machinists_add = 250
			building_employment_engineers_add = 250
		}
	}	
	
	state_modifiers = {
		workforce_scaled = {
			state_infrastructure_add = 15
			state_pollution_generation_add = 10
		}
	}
}

pm_automata_laborers_railways = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	unlocking_laws = {
		law_sapience_mechanim_compromise
		law_sapience_mechanim_unrecognized
	}
	unlocking_technologies = {
		early_mechanim		
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 6	
		}

		level_scaled = {
			building_employment_laborers_add = -3000
		}
	}
}

pm_automata_machinists_railways = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	unlocking_laws = {
		law_sapience_mechanim_compromise
		law_sapience_mechanim_unrecognized
	}
	unlocking_technologies = {
		advanced_mechanim			
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 10	
		}

		level_scaled = {
			building_employment_laborers_add = -3000
			building_employment_machinists_add = -1750
			building_employment_clerks_add = -250
		}
	}
}

pm_automata_laborers_railways_enforced = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	unlocking_laws = {
		law_sapience_mechanim_compromise_enforced
		law_sapience_mechanim_unrecognized_enforced
	}
	unlocking_technologies = {
		early_mechanim		
	}
    is_hidden_when_unavailable = yes
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 7	
		}

		level_scaled = {
			building_employment_laborers_add = -3000
		}
	}
}

pm_automata_machinists_railways_enforced = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	unlocking_laws = {
		law_sapience_mechanim_compromise_enforced
		law_sapience_mechanim_unrecognized_enforced
	}
	unlocking_technologies = {
		advanced_mechanim			
	}
	is_hidden_when_unavailable = yes
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 11	
		}

		level_scaled = {
			building_employment_laborers_add = -3000
			building_employment_machinists_add = -1750
			building_employment_clerks_add = -250
		}
	}
}

pm_manor_house_mage_ownership = {	#represents magical nobles. we can do adventurer guild and mage towers as a separate building proper
	texture = "gfx/interface/icons/production_method_icons/ownership_clergy.dds"

	disallowing_laws = {
		law_mundane_production
		law_artifice_only
	}

	building_modifiers = {
		level_scaled = {
			building_employment_mages_add = 100
		}
		unscaled = {
			building_mages_shares_add = 1
		}
	}
}
