﻿dhal_nikhuvad_survey_decision = {
	is_shown = {
		has_interest_marker_in_region = region_jasir_jadid
		NOT = { 
			has_variable = survey_dhal_nikhuvad_taken 
			has_global_variable = dhal_nikhuvad_canal_complete
		}
		OR = {
			country_rank = rank_value:great_power
			s:STATE_DHAL_NIKHUVAD = {
				any_scope_state = {
					owner = ROOT
				}
			}
		}
		s:STATE_DHAL_NIKHUVAD = {
			any_scope_state = {
				NOT = {
					any_scope_building = { 
						is_building_type = building_dhal_nikhuvad_canal 
					}
				}
			}
		}
	}

	possible = {
		has_technology_researched = colonization
		produced_bureaucracy > 1000
	}
	
	when_taken = {
		set_variable = {
			name = survey_dhal_nikhuvad_taken
			value = 0
		}
		add_modifier = {
			name = modifier_surveying_dhal_nikhuvad
		}
		add_journal_entry = {
			type = je_dhal_nikhuvad_survey
		}
	}

	ai_chance = {
		base = 0
		modifier = {
			trigger = { produced_bureaucracy > 5000 }
			add = 5
		}
		modifier = {
			trigger = {
				has_strategy = ai_strategy_armed_isolationism
			}
			add = -1000
		}		
		modifier = {
			OR = {
				is_diplomatic_play_committed_participant = yes
				is_at_war = yes
			}
			add = -1000
		}		
	}
}

stop_dhal_nikhuvad_survey_decision = {
	is_shown = {
		has_modifier = modifier_surveying_dhal_nikhuvad
	}
	possible = {
		has_modifier = modifier_surveying_dhal_nikhuvad
	}
	when_taken = {
		remove_variable = survey_dhal_nikhuvad_taken
		remove_modifier = modifier_surveying_dhal_nikhuvad
	}
	ai_chance = {
		base = 0
	}
}

dhal_nikhuvad_survey_purchase = {
	is_shown = {
		has_variable = dhal_nikhuvad_canal_purchase
		NOT = { 
			has_global_variable = dhal_nikhuvad_canal_purchase_var
			has_global_variable = dhal_nikhuvad_canal_complete
		}
		s:STATE_SINAI = {
			any_scope_state = {
				NOT = {
					any_scope_building = { 
						is_building_type = building_dhal_nikhuvad_canal 
					}
				}
				owner = {
					owns_treaty_port_in = STATE_DHAL_NIKHUVAD
					NOT = { 
						has_variable = dhal_nikhuvad_survey_complete 
					}
				}
			}
		}
	}

	possible = {
		gdp > 50000000
		p:xF94F81.state.owner = { # TODO - correct owner
			relations:root >= relations_threshold:cordial
		}
		NOT = { has_war_with = ROOT }
	}

	when_taken = {
		set_global_variable = dhal_nikhuvad_canal_purchase_var
		if = {
			limit = {
				s:STATE_DHAL_NIKHUVAD = {
					any_scope_state = {
						owner = {
							owns_treaty_port_in = STATE_DHAL_NIKHUVAD
							is_player = no 
						}
					}
				}
			}
			hidden_effect = {
				s:STATE_DHAL_NIKHUVAD = {
					random_scope_state = {
						limit = {
							owner = {
								owns_treaty_port_in = STATE_DHAL_NIKHUVAD 
							}
						}
						owner = {
							add_modifier = {
								name = dhal_nikhuvad_sale
								months = 120
							}
						}
					}
				}
			}
			s:STATE_DHAL_NIKHUVAD = {
				set_owner_of_provinces = {
					country = ROOT
					provinces = { xF94F81 }
				}
			}
			add_modifier = {
				name = dhal_nikhuvad_purchase
				months = 120
			}
		}
		else = {
			trigger_event = {
				id = canal_events.1
				days = 0
				popup = yes
			}
		}
	}

	ai_chance = {
		base = 0
		modifier = {
			trigger = { gold_reserves > 15000000 }
			add = 10
		}
		modifier = {
			trigger = { 
				weekly_net_fixed_income > 100000
				gold_reserves > 0
			}
			add = 10
		}		
		modifier = {
			trigger = {
				OR = {
					is_diplomatic_play_committed_participant = yes
					is_at_war = yes
				}	
			}
			add = -1000
		}		
	}
}