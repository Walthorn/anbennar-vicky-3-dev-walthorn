﻿
ig_trait_self_supplied_arms = {
	icon = "gfx/interface/icons/ig_trait_icons/veteran_consultation.dds"
	min_approval = happy
	
	modifier = {
		country_military_goods_cost_mult = -0.15
	}
}
