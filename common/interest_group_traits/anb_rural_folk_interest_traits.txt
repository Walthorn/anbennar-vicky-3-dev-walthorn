﻿ig_trait_heaven_upon_halann = {
	icon = "gfx/interface/icons/ig_trait_icons/be_fruitful_and_multiply.dds"
	min_approval = loyal
	
	modifier = {
		state_farmers_investment_pool_efficiency_mult = 0.2
	}
}