﻿CHARACTERS = {
	c:B47 ?= {
		create_character = { #once ambassador when Darran conquered the Dominion for a month, stayed there ever since
			first_name = Nestrin
			last_name = Farsight
			historical = yes
			culture = tuathak
			religion = autumn_court
			ruler = yes
			female = yes
			age = 55
			interest_group = ig_landowners
			ideology = ideology_aldanist
			traits = {
				persistent experienced_diplomat
			}
		}
	}
}
