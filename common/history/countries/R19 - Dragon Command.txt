﻿COUNTRIES = {
	c:R19 ?= {
		effect_starting_technology_tier_4_tech = yes
		add_technology_researched = tradition_of_equality

		add_technology_researched = urban_planning
		add_technology_researched = sericulture
		add_technology_researched = academia
		add_technology_researched = law_enforcement
		add_technology_researched = mandatory_service
		add_technology_researched = line_infantry
		
		effect_starting_politics_traditional = yes
		activate_law = law_type:law_stratocracy
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army
		activate_law = law_type:law_national_guard

		activate_law = law_type:law_interventionism
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_no_police

		activate_law = law_type:law_all_races_allowed
		activate_law = law_type:law_censorship
		activate_law = law_type:law_women_own_property # Not allowed women in workplace without voting
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_debt_slavery

		ig:ig_armed_forces = {
			add_ruling_interest_group = yes
		}

		add_modifier = { name = hobgoblin_statocracy_modifier }
		add_journal_entry = { type = je_command_splinter }
	}
}