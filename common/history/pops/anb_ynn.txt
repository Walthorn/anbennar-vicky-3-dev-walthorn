﻿POPS = {
	s:STATE_NIZVELS = {
		region_state:B29 = {
			create_pop = {
				culture = sarda
				size = 900000
				split_religion = {
					sarda = {
						ynn_river_worship = 0.7
						ravelian = 0.3
					}
				}
			}
			create_pop = {
				culture = steadsman
				size = 130000
			}
			create_pop = {
				culture = ynngarder_half_orc
				size = 50000
			}
		}
	}
	s:STATE_HRADAPOLERE = {
		region_state:B29 = {
			create_pop = {
				culture = sarda
				size = 1593750
				split_religion = {
					sarda = {
						ynn_river_worship = 0.9
						ravelian = 0.1
					}
				}
			}
			create_pop = {
				culture = dolindhan
				size = 122800
			}
			create_pop = {
				culture = veykodan
				size = 69750
			}
			create_pop = {
				culture = ynngarder_half_orc
				size = 93750
			}
			create_pop = {
				culture = east_ynnsman
				size = 43750
			}
			create_pop = {
				culture = horizon_elf
				size = 56240
			}
		}
	}
	s:STATE_VYCHVELS = {
		region_state:B29 = {
			create_pop = {
				culture = sarda
				size = 731250
				split_religion = {
					sarda = {
						ynn_river_worship = 0.75
						ravelian = 0.1
						corinite = 0.15
					}
				}
			}
			create_pop = {
				culture = veykodan
				size = 225000
			}
			create_pop = {
				culture = pipefoot_halfling
				size = 112500
			}
			create_pop = {
				culture = east_ynnsman
				size = 56250
			}
		}
	}
	s:STATE_YNNPADH = {
		region_state:B29 = {
			create_pop = {
				culture = sarda
				size = 343750
				split_religion = {
					sarda = {
						ynn_river_worship = 0.8
						ravelian = 0.2
					}
				}
			}
			create_pop = {
				culture = veykodan
				size = 187500
			}
			create_pop = {
				culture = boek
				size = 31250
			}
			create_pop = {
				culture = concordian
				size = 17460
			}
			create_pop = {
				culture = pipefoot_halfling
				size = 39250
			}
			create_pop = {
				culture = steadsman
				size = 31250
			}
		}
	}
	s:STATE_YRISRAD = {
		region_state:B29 = {
			create_pop = {
				culture = sarda
				size = 1837500
				split_religion = {
					sarda = {
						ynn_river_worship = 0.9
						ravelian = 0.1
					}
				}
			}
			create_pop = {
				culture = dolindhan
				size = 109552
			}
			create_pop = {
				culture = rzentur
				religion = ynn_river_worship
				size = 40140
			}
			create_pop = {
				culture = veykodan
				size = 142500
			}
			create_pop = {
				culture = east_ynnsman
				size = 161250
			}
			create_pop = {
				culture = pipefoot_halfling
				size = 151250
			}
			create_pop = {
				culture = ynngarder_half_orc
				size = 91250
			}
			create_pop = {
				culture = steel_dwarf
				size = 31250
			}
		}
	}
	s:STATE_VIZANIRZAG = {
		region_state:B31 = {
			create_pop = {
				culture = veykodan
				size = 468750
			}
			create_pop = {
				culture = sarda
				size = 93750
			}
			create_pop = {
				culture = pipefoot_halfling
				size = 62500
			}
			create_pop = {
				culture = teira
				size = 24000
			}
		}
	}
	s:STATE_LETHIR = {
		region_state:B31 = {
			create_pop = {
				culture = east_ynnsman
				size = 393750
			}
			create_pop = {
				culture = veykodan
				size = 84374
			}
			create_pop = {
				culture = sarda
				size = 56250
			}
			create_pop = {
				culture = pipefoot_halfling
				size = 28124
			}
			create_pop = {
				culture = teira
				size = 11200
			}
		}
	}
	s:STATE_CORINSFIELD = {
		region_state:B33 = {
			create_pop = {
				culture = east_ynnsman
				religion = corinite
				size = 1125000
			}
			create_pop = {
				culture = sarda
				size = 26250
			}
			create_pop = {
				culture = veykodan
				size = 23750
			}
		}
		region_state:B36 = {
			create_pop = {
				culture = east_ynnsman
				religion = corinite
				size = 37500
			}
			create_pop = {
				culture = pipefoot_halfling
				size = 15624
			}
			create_pop = {
				culture = steel_dwarf
				size = 8750
			}
			create_pop = {
				culture = sarda
				size = 1250
			}
			create_pop = {
				culture = veykodan
				size = 6250
			}
		}
	}
	s:STATE_WEST_TIPNEY = {
		region_state:B32 = { #Majority Halfling, followed by Ruinborn, human, horc, dwarf
			create_pop = {
				culture = pipefoot_halfling
				size = 618750
				split_religion = {
					pipefoot_halfling = {
						ynn_river_worship = 0.3
						corinite = 0.2
						ravelian = 0.5
					}
				}
			}
			create_pop = {
				culture = veykodan
				size = 85000
			}
			create_pop = {
				culture = sarda
				size = 66500
			}
			create_pop = {
				culture = teira
				size = 5000
			}
			create_pop = {
				culture = east_ynnsman
				size = 88750
				religion = ravelian
			}
			create_pop = {
				culture = east_ynnsman
				size = 76254
				religion = corinite
			}
			create_pop = {
				culture = freemarcher_half_orc
				size = 48500
			}
			create_pop = {
				culture = steel_dwarf
				size = 12500
			}
		}
	}
	s:STATE_OSINDAIN = {
		region_state:B35 = {
			create_pop = {
				culture = east_ynnsman
				size = 291874
			}
			create_pop = {
				culture = pipefoot_halfling
				size = 21874
			}
			create_pop = {
				culture = steel_dwarf
				size = 10680
			}
			create_pop = {
				culture = veykodan
				size = 43750
			}
			create_pop = {
				culture = teira
				size = 6400
			}
		}
	}
	s:STATE_NEW_HAVORAL = {
		region_state:B35 = {
			create_pop = {
				culture = east_ynnsman
				size = 337500
			}
			create_pop = {
				culture = steel_dwarf
				size = 18750
			}
			create_pop = {
				culture = veykodan
				size = 18750
			}
		}
	}
	s:STATE_ARGEZVALE = {
		region_state:B36 = {
			create_pop = {
				culture = steel_dwarf
				size = 562500
				split_religion = {
					steel_dwarf = {
						regent_court = 0.5
						ravelian = 0.5
					}
				}
			}
			create_pop = {
				culture = veykodan
				size = 152500
				split_religion = {
					veykodan = {
						rune_scribes = 0.4
						regent_court = 0.2
						ravelian = 0.4
					}
				}
			}
			create_pop = {
				culture = sarda
				size = 4750
			}
			create_pop = {
				culture = pipefoot_halfling
				size = 18750
			}
			create_pop = {
				culture = east_ynnsman
				religion = corinite
				size = 10840
			}
		}
	}
	s:STATE_NIZELYNN = {
		region_state:B29 = {
			create_pop = {
				culture = sarda
				size = 400000
				split_religion = {
					sarda = {
						ynn_river_worship = 0.8
						ravelian = 0.2
					}
				}
			}
			create_pop = {
				culture = epednar
				size = 70000
				religion = ynn_river_worship
			}
			create_pop = {
				culture = steadsman
				size = 37000
			}
			create_pop = {
				culture = ynngarder_half_orc
				size = 50000
			}
			create_pop = {
				culture = boek
				size = 16000
			}
		}
		region_state:B34 = {
			create_pop = {
				culture = sarda
				size = 30000
			}
			create_pop = {
				culture = epednar
				religion = ravelian
				size = 24000
			}
			create_pop = {
				culture = steadsman
				size = 135000
			}
			create_pop = {
				culture = ynngarder_half_orc
				size = 36000
			}
		}
	}
	s:STATE_CHIPPENGARD = {
		region_state:B30 = {
			create_pop = {
				culture = ynngarder_half_orc
				size = 247750
				split_religion = {
					ynngarder_half_orc = {
						ynn_river_worship = 0.35
						corinite = 0.5
						ravelian = 0.15
					}
				}
			}
			create_pop = {
				culture = steadsman
				size = 56250
				split_religion = {
					steadsman = {
						corinite = 0.5
						ravelian = 0.5
					}
				}
			}
			create_pop = {
				culture = sarda
				size = 37500
			}
			create_pop = {
				culture = epednar
				size = 21500
			}
		}
	}
	s:STATE_BEGGASLAND = {
		region_state:B34 = {
			create_pop = {
				culture = steadsman
				size = 482500
			}
			create_pop = {
				culture = epednar
				size = 32500
				split_religion = {
					epednar = {
						eotomolaque = 0.1
						ravelian = 0.9
					}
				}
			}
			create_pop = {
				culture = sarda
				size = 75000
			}
		}
	}
	s:STATE_PLUMSTEAD = {
		region_state:B41 = {
			create_pop = {
				culture = steadsman
				size = 705000
				split_religion = {
					steadsman = {
						corinite = 0.05
						ravelian = 0.95
					}
				}
			}
			create_pop = {
				culture = epednar
				religion = ravelian
				size = 175000
			}
			create_pop = {
				culture = sarda
				religion = ravelian
				size = 43750
			}
			create_pop = {
				culture = horizon_elf
				religion = ravelian
				size = 29750
			}
			create_pop = {
				culture = pipefoot_halfling
				size = 87500
				split_religion = {
					pipefoot_halfling = {
						corinite = 0.1
						ravelian = 0.9
					}
				}
			}
			create_pop = {
				culture = concordian
				size = 25000
			}
		}
	}
	s:STATE_TUSNATA = {
		region_state:B42 = {
			create_pop = {
				culture = steadsman
				split_religion = {
					steadsman = {
						corinite = 0.9
						ravelian = 0.1
					}
				}
				size = 50000
			}
			create_pop = {
				culture = epednar
				size = 30000
				split_religion = {
					epednar = {
						eotomolaque = 0.3
						corinite = 0.7
					}
				}
			}
		}
		region_state:B11 = {
			create_pop = {
				culture = epednar
				size = 18000
			}
		}
	}
	s:STATE_BORUCKY = {
		region_state:B40 = {
			create_pop = {
				culture = epednar
				size = 10000
				split_religion = {
					epednar = {
						eotomolaque = 0.1
						ravelian = 0.9
					}
				}
			}
		}
		region_state:B42 = {
			create_pop = {
				culture = steadsman
				split_religion = {
					steadsman = {
						ravelian = 0.7
						corinite = 0.3
					}
				}
				size = 120000
			}
			create_pop = {
				culture = epednar
				size = 20000
			}
		}
	}
	s:STATE_ARANTAS = {
		region_state:B40 = {
			create_pop = {
				culture = epednar
				size = 32500
			}
		}
		region_state:B37 = {
			create_pop = {
				culture = horizon_elf
				size = 5000
			}
			create_pop = {
				culture = epednar
				size = 2500
			}
		}
		region_state:B41 = {
			create_pop = {
				culture = steadsman
				size = 1970
			}
			create_pop = {
				culture = epednar
				size = 52500
				split_religion = {
					epednar = {
						eotomolaque = 0.5
						ravelian = 0.5
					}
				}
			}
		}
	}
	s:STATE_POSKAWA = {
		region_state:B40 = {
			create_pop = {
				culture = epednar
				size = 55000
			}
		}
		region_state:B43 = {
			create_pop = {
				culture = chiunife
				size = 12000
			}
			create_pop = {
				culture = epednar
				size = 3000
			}
		}
	}
	s:STATE_ELATHAEL = {
		region_state:B37 = {
			create_pop = {
				culture = horizon_elf
				size = 315000
			}
			create_pop = {
				culture = dustman
				size = 67500
				split_religion = {
					dustman = {
						ravelian = 0.6
						elven_forebears = 0.4
					}
				}
			}
			create_pop = {
				culture = epednar
				size = 33000
				split_religion = {
					epednar = {
						eotomolaque = 0.1
						elven_forebears = 0.9
					}
				}
			}
		}
		region_state:B40 = {
			create_pop = {
				culture = epednar
				size = 34500
			}
		}
	}
	s:STATE_EPADARKAN = {
		region_state:B46 = {
			create_pop = {
				culture = sarda
				size = 212500
			}
			create_pop = {
				culture = dolindhan
				size = 37400
			}
			create_pop = {
				culture = dustman
				size = 18500
			}
		}
		region_state:B45 = {
			create_pop = {
				culture = sarda
				size = 350000
			}
			create_pop = {
				culture = dustman
				size = 50000
			}
		}
	}
	s:STATE_TELLUMTIR = {
		region_state:B91 = {
			create_pop = {
				culture = dustman
				size = 440000
			}
			create_pop = {
				culture = epednar
				size = 60000
				split_religion = {
					epednar = {
						eotomolaque = 0.2
						ravelian = 0.8
					}
				}
			}
		}
	}
	s:STATE_UZOO = {
		region_state:B91 = {
			create_pop = {
				culture = dustman
				size = 40000
			}
			create_pop = {
				culture = epednar
				size = 45000
				split_religion = {
					epednar = {
						eotomolaque = 0.5
						ravelian = 0.5
					}
				}
			}
		}
		region_state:B40 = {
			create_pop = {
				culture = epednar
				size = 135000
			}
		}
	}
	s:STATE_ARGANJUZORN = {
		region_state:B47 = {
			create_pop = {
				culture = dolindhan
				size = 512500
			}
			create_pop = {
				culture = epednar
				size = 19500
			}
			create_pop = {
				culture = dustman
				size = 53500
			}
			create_pop = {
				culture = sarda
				size = 12500
			}
			create_pop = {
				culture = tuathak
				size = 75000
			}
		}
		region_state:B46 = {
			create_pop = {
				culture = sarda
				size = 62500
			}
			create_pop = {
				culture = dolindhan
				size = 12500
			}
		}
	}
	s:STATE_EBENMAS = {
		region_state:B91 = {
			create_pop = {
				culture = dustman
				size = 497500
			}
			create_pop = {
				culture = epednar
				size = 52500
				split_religion = {
					epednar = {
						eotomolaque = 0.2
						ravelian = 0.8
					}
				}
			}
		}
	}
	s:STATE_YECKUNIA = {
		region_state:B91 = {
			create_pop = {
				culture = dustman
				size = 9000
			}
			create_pop = {
				culture = epednar
				split_religion = {
					epednar = {
						eotomolaque = 0.5
						ravelian = 0.5
					}
				}
				size = 1500
			}
		}
		region_state:G14 = {
			create_pop = {
				culture = chiunife
				size = 42000
			}
		}
		region_state:G13 = {
			create_pop = {
				culture = epednar
				size = 27500
			}
		}
	}
	s:STATE_CASNAROG = {
		region_state:G13 = {
			create_pop = {
				culture = chiunife
				size = 20500
			}
			create_pop = {
				culture = epednar
				size = 37500
			}
		}
	}
	s:STATE_POMVASONN = {
		region_state:B49 = {
			create_pop = {
				culture = rzentur
				size = 1150000
			}
			create_pop = {
				culture = epednar
				size = 10000
			}
			create_pop = {
				culture = dustman
				size = 30000
			}
			create_pop = {
				culture = vrendzani_kobold
				size = 100000
				split_religion = {
					vrendzani_kobold = {
						kobold_dragon_cult = 0.2
						drozma_tur = 0.8
					}
				}
			}
		}
	}
	s:STATE_MOCEPED = {
		region_state:B49 = {
			create_pop = {
				culture = dolindhan
				size = 614374
				split_religion = {
					dolindhan = {
						ynn_river_worship = 0.25
						drozma_tur = 0.75
					}
				}
			}
			create_pop = {
				culture = rzentur
				size = 103124
			}
			create_pop = {
				culture = dustman
				size = 30000
			}
		}
	}
	s:STATE_VIZKALADR = {
		region_state:B49 = {
			create_pop = {
				culture = rzentur
				size = 2025000
				split_religion = {
					rzentur = {
						drozma_tur = 0.95
						corinite = 0.05
					}
				}
			}
			create_pop = {
				culture = rzentur
				religion = vechnogejzn
				size = 50000
			}
			create_pop = {
				culture = dustman
				religion = corinite
				size = 150000
			}
			create_pop = {
				culture = vrendzani_kobold
				size = 180000
				split_religion = {
					vrendzani_kobold = {
						kobold_dragon_cult = 0.2
						drozma_tur = 0.8
					}
				}
			}
		}
	}
	s:STATE_GOMOSENGHA = {
		region_state:B49 = {
			create_pop = {
				culture = rzentur
				size = 680000
				split_religion = {
					rzentur = {
						drozma_tur = 0.9
						corinite = 0.1
					}
				}
			}
			create_pop = {
				culture = rzentur
				religion = vechnogejzn
				size = 54000
			}
			create_pop = {
				culture = vrendzani_kobold
				size = 40000
				split_religion = {
					vrendzani_kobold = {
						kobold_dragon_cult = 0.2
						drozma_tur = 0.8
					}
				}
			}
			create_pop = {
				culture = vrendzani_kobold
				size = 210000
				pop_type = slaves
				split_religion = {
					vrendzani_kobold = {
						kobold_dragon_cult = 0.5
						drozma_tur = 0.5
					}
				}
			}
		}
	}
	s:STATE_ARSERGOZHAR = {
		region_state:B50 = {
			create_pop = {
				culture = rzentur
				religion = vechnogejzn
				size = 62500
			}
		}
	}
	s:STATE_JUZONDEZAN = {
		region_state:B49 = {
			create_pop = {
				culture = rzentur
				size = 730000
				split_religion = {
					rzentur = {
						drozma_tur = 0.85
						corinite = 0.15
					}
				}
			}
			create_pop = {
				culture = dolindhan
				size = 545000
				religion = drozma_tur
			}
			create_pop = {
				culture = dustman
				religion = corinite
				size = 125000
			}
			create_pop = {
				culture = vrendzani_kobold
				size = 75000
				religion = drozma_tur
			}
		}
	}
	s:STATE_USLAD = {
		region_state:B39 = {
			create_pop = {
				culture = dolindhan
				size = 675000
			}
			create_pop = {
				culture = rzentur
				size = 39000
			}
			create_pop = {
				culture = sarda
				size = 125000
			}
			create_pop = {
				culture = east_ynnsman
				religion = corinite
				size = 33000
			}
			create_pop = {
				culture = pipefoot_halfling
				size = 45000
			}
			create_pop = {
				culture = cliff_gnome
				size = 27500
			}
		}
		region_state:B52 = {
			create_pop = {
				culture = dolindhan
				size = 510500
			}
		}
	}
	s:STATE_ROGAIDHA = {
		region_state:B49 = {
			create_pop = {
				culture = dolindhan
				size = 1687500
				split_religion = {
					dolindhan = {
						drozma_tur = 0.25
						corinite = 0.05
						ynn_river_worship = 0.7
					}
				}
			}
			create_pop = {
				culture = rzentur
				size = 187500
			}
			create_pop = {
				culture = dustman
				size = 25200
				religion = corinite
			}
		}
	}
	s:STATE_BRELAR = {
		region_state:B49 = {
			create_pop = {
				culture = rzentur
				religion = vechnogejzn
				size = 62500
			}
			create_pop = {
				culture = dolindhan
				size = 562500
				split_religion = {
					dolindhan = {
						ynn_river_worship = 0.85
						corinite = 0.15
					}
				}
			}
		}
	}
	s:STATE_VERZEL = {
		region_state:B49 = {
			create_pop = {
				culture = rzentur
				size = 225000
				split_religion = {
					rzentur = {
						vechnogejzn = 0.4
						drozma_tur = 0.3
						corinite = 0.3
					}
				}
			}
			create_pop = {
				culture = dolindhan
				size = 190000
				split_religion = {
					dolindhan = {
						ynn_river_worship = 0.1
						drozma_tur = 0.65
						corinite = 0.25
					}
				}
			}
			create_pop = {
				culture = dustman
				religion = corinite
				size = 135000
			}
		}
	}
	s:STATE_VITREYNN = {
		region_state:B38 = {
			create_pop = {
				culture = dolindhan
				size = 1085000
			}
		}
	}
	s:STATE_VARBUKLAND = {
		region_state:B53 = {
			create_pop = {
				culture = freemarcher_half_orc
				size = 452000
			}
			create_pop = {
				culture = cursed_one
				size = 25000
			}
		}
	}
	s:STATE_KEWDHEMR = {
		region_state:B54 = {
			create_pop = {
				culture = cursed_one
				size = 22874
			}
		}
		region_state:B55 = {
			create_pop = {
				culture = cursed_one
				size = 21624
			}
		}
		region_state:B57 = {
			create_pop = {
				culture = cursed_one
				size = 3000
			}
		}
		region_state:B53 = {
			create_pop = {
				culture = freemarcher_half_orc
				size = 21000
			}
			create_pop = {
				culture = cursed_one
				size = 14000
			}
		}
	}
	s:STATE_HEHADEDPAR = {
		region_state:B63 = {
			create_pop = {
				culture = cursed_one
				size = 17650
			}
		}
		region_state:B56 = {
			create_pop = {
				culture = cursed_one
				size = 36250
			}
		}
		region_state:B57 = {
			create_pop = {
				culture = cursed_one
				size = 23874
			}
		}
		region_state:B58 = {
			create_pop = {
				culture = cursed_one
				size = 40900
			}
		}
		region_state:G08 = {
			create_pop = {
				culture = cliff_gnome
				size = 3150
			}
			create_pop = {
				culture = themarian
				size = 20174
			}
			create_pop = {
				culture = reverian
				size = 17750
			}
		}
	}
	s:STATE_ESIMOINE = {
		region_state:B43 = {
			create_pop = {
				culture = chiunife
				size = 5500
			}
			create_pop = {
				culture = epednar
				size = 2000
			}
		}
		region_state:B11 = {
			create_pop = {
				culture = haraf_ne
				size = 20000
			}
			create_pop = {
				culture = epednar
				size = 5000
			}
		}
        region_state:G15 = {
			create_pop = {
				culture = chiunife
				size = 9500
			}
			create_pop = {
				culture = epednar
				size = 3000
			}
		}
		region_state:B42 = {
			create_pop = {
				culture = steadsman
				size = 257500
				split_religion = {
					steadsman = {
						corinite = 0.6
						ravelian = 0.4
					}
				}
			}
			create_pop = {
				culture = undercliff_orc
				size = 35000
				pop_type = slaves
			}
			create_pop = {
				culture = epednar
				size = 20000
				religion = corinite
			}
		}
	}
	s:STATE_MORGANIA = {
		region_state:B43 = {
			create_pop = {
				culture = chiunife
				size = 7400
			}
			create_pop = {
				culture = haraf_ne
				size = 1400
			}
		}
        region_state:G14 = {
			create_pop = {
				culture = chiunife
				size = 5600
			}
			create_pop = {
				culture = haraf_ne
				size = 1200
			}
            create_pop = {
				culture = fograc
				size = 5400
			}
		}
		region_state:B11 = {
			create_pop = {
				culture = haraf_ne
				size = 10800
			}
		}
		region_state:B79 = {
			create_pop = {
				culture = fograc
				size = 1200
			}
			create_pop = {
				culture = haraf_ne
				size = 3200
			}
		}
	}
	s:STATE_TASPAS = {
		region_state:B43 = {
			create_pop = {
				culture = chiunife
				size = 7125
			}
			create_pop = {
				culture = epednar
				size = 250
			}
		}
        region_state:B40 = {
            create_pop = {
				culture = epednar
				size = 750
			}
            create_pop = {
				culture = chiunife
				size = 125
			}
        }
        region_state:G14 = {
			create_pop = {
				culture = chiunife
				size = 7125
			}
		}
	}
	s:STATE_EKRSOKA = {
		region_state:B49 = {
			create_pop = {
				culture = epednar
				size = 30000
				religion = drozma_tur
			}
			create_pop = {
				culture = rzentur
				size = 8000
			}
			create_pop = {
				culture = dustman
				size = 15000
			}
		}
		region_state:G13 = {
			create_pop = {
				culture = epednar
				size = 45000
			}
			create_pop = {
				culture = dustman
				size = 14000
			}
		}
        region_state:B91 = {
			create_pop = {
				culture = epednar
				size = 2000
			}
			create_pop = {
				culture = dustman
				size = 10000
			}
		}
	}
}