﻿MILITARY_FORMATIONS = {
	c:C33 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_devand
			name = "keyolion division"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_KEYOLION
				count = 2
				}
            }

			create_military_formation = {
				type = fleet
				hq_region = sr:region_devand

				combat_unit = {
					type = unit_type:combat_unit_type_frigate
					state_region = s:STATE_KEYOLION
					count = 2
				}
			}
       }
       c:C35 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_devand
			name = "Besolakian lions"

				combat_unit = {
					type = unit_type:combat_unit_type_irregular_infantry
					state_region = s:STATE_BESOLAKI
					count = 2
				}
            }

			create_military_formation = {
				type = fleet
				hq_region = sr:region_devand

				combat_unit = {
					type = unit_type:combat_unit_type_frigate
					state_region = s:STATE_BESOLAKI
					count = 4
				}
			}
       }
       c:C30 ?= {
            create_military_formation = {
                    type = army
                    hq_region = sr:region_alecand
                    name = "ultramarine legion"
                    combat_unit = {
                    	type = unit_type:combat_unit_type_dragoons
                    	state_region = S:STATE_NORTIKAN
                    	count = 2
                    }
                    combat_unit = {
                    	type = unit_type:combat_unit_type_line_infantry
                    	state_region = S:STATE_NORTHERN_KHEIONS
                    	count = 3
                    }
                    combat_unit = {
                    	type = unit_type:combat_unit_type_cannon_artillery
                    	state_region = S:STATE_SOUTHERN_KHEIONS
                    	count = 3
                    }
                    combat_unit = {
                    	type = unit_type:combat_unit_type_line_infantry
                    	state_region = S:STATE_CENTRAL_KHEIONS
                    	count = 3
                    }
                    combat_unit = {
                    	type = unit_type:combat_unit_type_cannon_artillery
                    	state_region = S:STATE_CENTRAL_KHEIONS
                    	count = 2
                    }
                    combat_unit = {
                    	type = unit_type:combat_unit_type_line_infantry
                    	state_region = S:STATE_SOUTIKAN
                    	count = 2
                    }
                }
            create_military_formation = {
				type = fleet
				hq_region = sr:region_alecand
				name = "the grand fleet"

				combat_unit = {
					type = unit_type:combat_unit_type_man_o_war
					state_region = s:STATE_NORTHERN_KHEIONS
					count = 1
				}

				combat_unit = {
					type = unit_type:combat_unit_type_frigate
					state_region = s:STATE_CENTRAL_KHEIONS
					count = 4
				}

				combat_unit = {
					type = unit_type:combat_unit_type_frigate
					state_region = s:STATE_SOUTHERN_KHEIONS
					count = 3
				}
			}
        }
        c:C34 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_devand
			name = "Enion free lancers"

				combat_unit = {
					type = unit_type:combat_unit_type_line_infantry
					state_region = s:STATE_ENEION
					count = 5
				}
            }

			create_military_formation = {
				type = fleet
				hq_region = sr:region_devand

				combat_unit = {
					type = unit_type:combat_unit_type_frigate
					state_region = s:STATE_ENEION
					count = 2
				}
			}
       }
       c:C36 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_devand
			name = "DEFENSE FORCE"

				combat_unit = {
					type = unit_type:combat_unit_type_line_infantry
					state_region = s:STATE_APIKHOXI
					count = 4
				}
            }

			create_military_formation = {
				type = fleet
				hq_region = sr:region_devand

				combat_unit = {
					type = unit_type:combat_unit_type_frigate
					state_region = s:STATE_APIKHOXI
					count = 2
				}
			}
       }
       c:C70 ?= {
               create_military_formation = {
				type = army
				hq_region = sr:region_devand
				name = "Amgremos grand army"

				combat_unit = {
					type = unit_type:combat_unit_type_line_infantry
					state_region = s:STATE_AMGREMOS
					count = 2
				}
				combat_unit = {
					type = unit_type:combat_unit_type_line_infantry
					state_region = s:STATE_VOLITHORAM
					count = 2
				}
            }

			create_military_formation = {
				type = fleet
				hq_region = sr:region_devand

				combat_unit = {
					type = unit_type:combat_unit_type_frigate
					state_region = s:STATE_AMGREMOS
					count = 5
				}
			}
       }
       c:C37 ?= {
       	        create_military_formation = {
			type = army
			hq_region = sr:region_devand
			name = "Deyions loyal servants"

				combat_unit = {
					type = unit_type:combat_unit_type_line_infantry
					state_region = s:STATE_OKTIAMOTON
					count = 1
				}
				combat_unit = {
					type = unit_type:combat_unit_type_line_infantry
					state_region = s:STATE_DEYEION
					count = 4
				}
            }

			create_military_formation = {
				type = fleet
				hq_region = sr:region_devand

				combat_unit = {
					type = unit_type:combat_unit_type_frigate
					state_region = s:STATE_OKTIAMOTON
					count = 2
				}

				combat_unit = {
					type = unit_type:combat_unit_type_frigate
					state_region = s:STATE_DEYEION
					count = 2
				}
			}
       }
       c:C38 ?= {
       	        create_military_formation = {
			type = army
			hq_region = sr:region_devand
			name = "AMGREMOS GRAND ARMY"

				combat_unit = {
					type = unit_type:combat_unit_type_irregular_infantry
					state_region = s:STATE_WESTERN_MTEIBHARA
					count = 2
				}
				combat_unit = {
					type = unit_type:combat_unit_type_irregular_infantry
					state_region = s:STATE_EASTERN_MTEIBHARA
					count = 2
				}
            }
       }
       c:C39 ?= {
       	    create_military_formation = {
				type = army
				hq_region = sr:region_devand
				name = "Ameions great conquest host"

				combat_unit = {
					type = unit_type:combat_unit_type_line_infantry
					state_region = s:STATE_AMEION
					count = 8
				}
				combat_unit = {
					type = unit_type:combat_unit_type_cannon_artillery
					state_region = s:STATE_MESOKTI
					count = 4
				}
				combat_unit = {
					type = unit_type:combat_unit_type_cannon_artillery
					state_region = s:STATE_AMEION
					count = 4
				}
            }

			create_military_formation = {
				type = fleet
				hq_region = sr:region_devand

				combat_unit = {
					type = unit_type:combat_unit_type_frigate
					state_region = s:STATE_AMEION
					count = 8
				}

				combat_unit = {
					type = unit_type:combat_unit_type_frigate
					state_region = s:STATE_MESOKTI
					count = 4
				}

				combat_unit = {
					type = unit_type:combat_unit_type_frigate
					state_region = s:STATE_PARAIDAR
					count = 4
				}
			}

			create_military_formation = {
				type = fleet
				hq_region = sr:region_devand

				combat_unit = {
					type = unit_type:combat_unit_type_frigate
					state_region = s:STATE_PARAIDAR
					count = 4
				}
			}
       }
       c:C53 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_irregular_infantry
       	       		state_region = s:STATE_MESOKTI
       	       		count = 6
       	       	        }
       	        }
       }
       c:C55 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_irregular_infantry
       	       		state_region = s:STATE_MESOKTI
       	       		count = 8
       	       	        }
       	        }
       }
       c:C40 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_irregular_infantry
       	       		state_region = s:STATE_CLEMATAR
       	       		count = 11
       	       	        }
       	        }
       }
       c:C43 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_irregular_infantry
       	       		state_region = s:STATE_ENLARMAI
       	       		count = 7
       	       	        }
       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_irregular_infantry
       	       		state_region = s:STATE_NANRU_NAKAR
       	       		count = 3
       	       	        }
       	        }
       }
       c:C44 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       			type = unit_type:combat_unit_type_irregular_infantry
       	       			state_region = s:STATE_IRON_HILLS
       	       			count = 4
       	       	        }
       	        }
       }
       c:C66 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       			type = unit_type:combat_unit_type_irregular_infantry
       	       			state_region = s:STATE_ENLARMAI
       	       			count = 5
       	       	        }
       	        }
       }
       c:C45 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_irregular_infantry
       	       		state_region = s:STATE_NANRU_NAKAR
       	       		count = 12
       	       	        }
       	        }
       }
       c:C45 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_irregular_infantry
       	       		state_region = s:STATE_NANRU_NAKAR
       	       		count = 12
       	       	        }
       	        }
       }
       c:C63 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_irregular_infantry
       	       		state_region = s:STATE_NANRU_NAKAR
       	       		count = 3
       	       	        }
       	        }
       }
       c:C65 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_irregular_infantry
       	       		state_region = s:STATE_NANRU_NAKAR
       	       		count = 4
       	       	        }
       	        }
       }
       c:C60 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       				type = unit_type:combat_unit_type_line_infantry
       	       				state_region = s:STATE_NEOR_EMPKEIOS
       	       				count = 5
       	       	        }
       	       	        combat_unit = {
       	       	        	type = unit_type:combat_unit_type_cannon_artillery
       	       	        	state_region = s:STATE_NEOR_EMPKEIOS
       	       	        	count = 2
       	       	        }
       	        }

				create_military_formation = {
					type = fleet
					hq_region = sr:region_taychend

					combat_unit = {
						type = unit_type:combat_unit_type_frigate
						state_region = s:STATE_NEOR_EMPKEIOS
						count = 4
					}
				}
       }
       c:C46 ?= {
       	       create_military_formation = {
       	       	    type = army
       	       	    hq_region = sr:region_taychend

       	       	    combat_unit = {
       	       			type = unit_type:combat_unit_type_line_infantry
       	       			state_region = s:STATE_KLERECHEND
       	       			count = 8
       	       	    }
       	       	    combat_unit = {
       	       			type = unit_type:combat_unit_type_line_infantry
       	       			state_region = s:STATE_NEOR_EMPKEIOS
       	       			count = 2
       	       	    }
       	       	    combat_unit = {
       	       			type = unit_type:combat_unit_type_line_infantry
       	       			state_region = s:STATE_NYMBHAVA
       	       			count = 1
       	       	    }
       	        }

				create_military_formation = {
					type = fleet
					hq_region = sr:region_taychend

					combat_unit = {
						type = unit_type:combat_unit_type_frigate
						state_region = s:STATE_KLERECHEND
						count = 7
					}
				}
       }
       c:C56 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_irregular_infantry
       	       		state_region = s:STATE_KLERECHEND
       	       		count = 3
       	       	        }
       	        }
       }
       c:C57 ?= {
		create_military_formation = {
			type = army
			hq_region = sr:region_taychend

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_KLERECHEND
				count = 6
			}
               }
       }
       c:C47 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_irregular_infantry
       	       		state_region = s:STATE_NYMBHAVA
       	       		count = 8
       	       	        }
       	        }
       }
       c:C58 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_irregular_infantry
       	       		state_region = s:STATE_NYMBHAVA
       	       		count = 4
       	       	        }
       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_irregular_infantry
       	       		state_region = s:STATE_IRON_HILLS
       	       		count = 5
       	       	        }
       	        }
       }
       c:C59 ?= {
       	       create_military_formation = {
       	       	    type = army
       	       	    hq_region = sr:region_taychend

       	       	    combat_unit = {
       	       			type = unit_type:combat_unit_type_irregular_infantry
       	       			state_region = s:STATE_NYMBHAVA
       	       			count = 4
       	       	    }
       	        }
       }
       c:C50 ?= {
       	       create_military_formation = {
       	       	     type = army
       	       	    hq_region = sr:region_taychend

       	       	     combat_unit = {
       	       			type = unit_type:combat_unit_type_line_infantry
       	       			state_region = s:STATE_DEGITHION
       	       			count = 10
       	       	    }
       	       	     combat_unit = {
       	       	        type = unit_type:combat_unit_type_cannon_artillery
       	       	        state_region = s:STATE_DEGITHION
       	       	        count = 4
       	       	    }
       	        }

				create_military_formation = {
					type = fleet
					hq_region = sr:region_taychend

					combat_unit = {
						type = unit_type:combat_unit_type_frigate
						state_region = s:STATE_DEGITHION
						count = 11
					}
				}
       }
       c:C61 ?= {
       	       create_military_formation = {
       	       	    type = army
       	       	    hq_region = sr:region_taychend

       	       	    combat_unit = {
       	       			type = unit_type:combat_unit_type_line_infantry
       	       			state_region = s:STATE_DEGITHION
       	       			count = 4
       	       	    }
       	        }
       }
       c:C49 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_irregular_infantry
       	       		state_region = s:STATE_ORENKORAIM
       	       		count = 4
       	       	        }
       	        }
       }
       c:C48 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_irregular_infantry
       	       		state_region = s:STATE_ORENKORAIM
       	       		count = 9
       	       	        }
       	        }
       }
       c:C62 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_irregular_infantry
       	       		state_region = s:STATE_ORENKORAIM
       	       		count = 6
       	       	        }
       	        }
       }
       c:C68 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_irregular_infantry
       	       		state_region = s:STATE_EAST_VEYII_SIKARHA
       	       		count = 4
       	       	        }
       	        }
       }
       c:C41 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_irregular_infantry
       	       		state_region = s:STATE_WEST_VEYII_SIKARHA
       	       		count = 3
       	       	        }
       	        }
       }
       c:C52 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_irregular_infantry
       	       		state_region = s:STATE_WEST_VEYII_SIKARHA
       	       		count = 3
       	       	        }
       	        }
       }
       c:C55 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_line_infantry
       	       		state_region = s:STATE_MESOKTI
       	       		count = 8
       	       	        }
       	        }
       }
       c:C69 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_irregular_infantry
       	       		state_region = s:STATE_SARIHADDU
       	       		count = 4
       	       	        }
       	        }
       }
       c:C67 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_irregular_infantry
       	       		state_region = s:STATE_SARIHADDU
       	       		count = 4
       	       	        }
       	        }
       }
       c:C54 ?= {
       	       create_military_formation = {
       	       	        type = army
       	       	        hq_region = sr:region_taychend

       	       	        combat_unit = {
       	       		type = unit_type:combat_unit_type_irregular_infantry
       	       		state_region = s:STATE_CAERGARAEN
       	       		count = 9
       	       	        }
       	        }
       }
       c:C32 ?= {
       	       create_military_formation = {
       	       	    type = army
       	       	    hq_region = sr:region_devand

       	       	    combat_unit = {
       	       			type = unit_type:combat_unit_type_line_infantry
       	       			state_region = s:STATE_EMPKEIOS
       	       			count = 1
       	       	    }
       	        }

				create_military_formation = {
					type = fleet
					hq_region = sr:region_devand

					combat_unit = {
						type = unit_type:combat_unit_type_frigate
						state_region = s:STATE_EMPKEIOS
						count = 3
					}
				}
       }
    c:C31 ?= {
       	create_military_formation = {
       		type = army
       		hq_region = sr:region_devand 
       		combat_unit = {
       			type=unit_type:combat_unit_type_irregular_infantry
       			state_region = s:STATE_ANISIKHEION
       			count = 1
       		}
       	}

		create_military_formation = {
			type = fleet
			hq_region = sr:region_devand

			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_ANISIKHEION
				count = 2
			}
		}
    }
}