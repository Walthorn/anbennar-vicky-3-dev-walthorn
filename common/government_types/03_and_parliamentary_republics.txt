﻿gov_federal_concord = {
	transfer_of_power = presidential_elective

	male_ruler = "RULER_TITLE_CHANCELLOR"
	female_ruler = "RULER_TITLE_CHANCELLOR"
	
	possible = {
		exists = c:B98
		c:B98 = ROOT
		has_law = law_type:law_parliamentary_republic
		country_has_voting_franchise = yes
	}

	on_government_type_change = {
		change_to_presidential_elective = yes
	}
	on_post_government_type_change = {
		post_change_to_presidential_elective = yes
	}
}

gov_artificer_triumvirate = {
	transfer_of_power = dictatorial

	male_ruler = "RULER_DIRECTOR_OF_FOREIGN_AFFAIRS"
	female_ruler = "RULER_DIRECTOR_OF_FOREIGN_AFFAIRS"
	
	possible = {
		exists = c:B07
		c:B07 = ROOT
		has_law = law_type:law_parliamentary_republic
		has_law = law_type:law_technocracy
	}

	on_government_type_change = {
		change_to_dictatorial = yes
	}
	on_post_government_type_change = {
		post_change_to_dictatorial = yes
	}
}

gov_blackpowder_republic = {
	transfer_of_power = presidential_elective
	new_leader_on_reform_government = no

	male_ruler = "RULER_TITLE_PRESIDENT"
	female_ruler = "RULER_TITLE_PRESIDENT"

	possible = {
		has_law = law_type:law_parliamentary_republic
		country_has_voting_franchise = yes
		OR = {
			has_law = law_type:law_artifice_encouraged
			has_law = law_type:law_artifice_only
		}
	}

	on_government_type_change = {
		change_to_presidential_elective = yes
	}
	on_post_government_type_change = {
		post_change_to_presidential_elective = yes
	}
}

gov_matriarchal_republic_parlament = {
	transfer_of_power = parliamentary_elective
	new_leader_on_reform_government = yes

	male_ruler = "RULER_MATRIARCH"
	female_ruler = "RULER_MATRIARCH"

	possible = {
		has_law = law_type:law_parliamentary_republic
		has_variable = is_matriarchy_var
		NOT = { has_law = law_type:law_womens_suffrage }
	}

	on_government_type_change = {
		change_to_parliamentary_elective = yes
	}
	on_post_government_type_change = {
		post_change_to_parliamentary_elective = yes
	}
}
