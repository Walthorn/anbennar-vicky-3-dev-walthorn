﻿#	Example:
# 
#	example_trigger = {
#		x = 100
#		y = 50
#	}
#
#
#	In a script file:
#
#	effect = {
#		example_trigger = yes
#	}
#

has_magocrat_ideology = {
	OR = {
		has_ideology = ideology:ideology_magocrat
		# has_ideology = ideology:ideology_bonapartist	#for future ones
		# has_ideology = ideology:ideology_legitimist	
	}
}