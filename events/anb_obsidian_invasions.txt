﻿namespace = obsidian_invasions


#third invasion reinforcements
obsidian_invasions.1 = {
	type = country_event
	placement = root
	
	title = obsidian_invasions.1.t
	desc =  obsidian_invasions.1.d
	flavor = obsidian_invasions.1.f
	
	event_image = {
		video = "unspecific_devastation"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_map.dds"
	
	duration = 3
	
	immediate = {
		create_character = {
			first_name = "Morzad"
			last_name = "Blackbarrel"
			historical = yes
			age = 51
			ig_leader = yes
			interest_group = ig_armed_forces
			ideology = ideology_jingoist
			traits = {
				direct
				wrathful
				experienced_artillery_commander
			}
			save_scope_as = herud
		}
	}

	option = { 
		name = obsidian_invasions.1.a
		default_option = yes

		s:STATE_VERKAL_SKOMDIHR = {
			region_state:D15 = {
				create_pop = {
					culture = obsidian_dwarf
					size = 800000
				}
				create_pop = {
					pop_type = officers
					culture = obsidian_dwarf
					size = 100000
				}
				create_pop = {
					pop_type = soldiers
					culture = obsidian_dwarf
					size = 600000
				}
				hidden_effect = {
					random_scope_building = {
						limit = { is_building_type = building_barracks }
						add_modifier = {
							name = obsidian_invasion_barracks_training
							weeks = 9
						}
					}
				}
			}
		}
		custom_tooltip = obsidian_invasions.1.tooltip

		hidden_effect = {
			create_military_formation = {
				type = army
				hq_region = sr:region_serpentreach
				name = "The Third Legion"
	
				combat_unit = {
					type = unit_type:combat_unit_type_line_infantry
					state_region = s:STATE_VERKAL_SKOMDIHR
					count = 35
				}
	
				combat_unit = {
					type = unit_type:combat_unit_type_mobile_artillery
					state_region = s:STATE_VERKAL_SKOMDIHR
					count = 25
				}
				save_scope_as = obsidian_reinforcements
			}
			create_character = {
				is_general = yes
				commander_rank = commander_rank_1
				save_scope_as = legion_commander_1
			}
			create_character = {
				is_general = yes
				commander_rank = commander_rank_1
				save_scope_as = legion_commander_2
			}
		}
		scope:herud = { set_character_as_ruler = yes add_character_role = general add_commander_rank = 4 }
	}
	after = {
		scope:herud = { transfer_to_formation = scope:obsidian_reinforcements }
		scope:legion_commander_1 = { transfer_to_formation = scope:obsidian_reinforcements }
		scope:legion_commander_2 = { transfer_to_formation = scope:obsidian_reinforcements }
		scope:obsidian_reinforcements = { fully_mobilize_army = yes add_organization = 95 }
	}
}

#third invasion complete
obsidian_invasions.2 = {
	type = country_event
	placement = root
	
	title = obsidian_invasions.2.t
	desc =  obsidian_invasions.2.d
	flavor = obsidian_invasions.2.f
	
	event_image = {
		video = "unspecific_devastation"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_map.dds"
	
	duration = 3
	
	option = { 
		name = obsidian_invasions.2.a
		default_option = yes
		
		s:STATE_VERKAL_SKOMDIHR = {
			add_homeland = cu:obsidian_dwarf
			remove_homeland = cu:flint_dwarf
		}
		s:STATE_ORCSFALL = { 
			remove_homeland = cu:flint_dwarf
		}
		s:STATE_ARGROD = {
			add_homeland = cu:obsidian_dwarf
			remove_homeland = cu:flint_dwarf
			remove_homeland = cu:garnet_dwarf
		}
		s:STATE_OVDAL_LODHUM = {
			add_homeland = cu:obsidian_dwarf
			remove_homeland = cu:garnet_dwarf
		}
		s:STATE_WINDING_CAVERNS = { 
			remove_homeland = cu:garnet_dwarf
		}
		s:STATE_ARG_ORDSTUN = {
			add_homeland = cu:obsidian_dwarf
			remove_homeland = cu:diamond_dwarf
		}
		s:STATE_DIAMOND_QUARRY = { 
			remove_homeland = cu:diamond_dwarf
		}
		s:STATE_ARGROD_TERMINUS = {
			add_homeland = cu:obsidian_dwarf
			remove_homeland = cu:diamond_dwarf
			remove_homeland = cu:marble_dwarf
			remove_homeland = cu:cobalt_dwarf
		}
		s:STATE_ORLGHELOVAR = {
			add_homeland = cu:obsidian_dwarf
			remove_homeland = cu:cobalt_dwarf
		}
		s:STATE_SHAZSTUNDIHR = {
			add_homeland = cu:obsidian_dwarf
			remove_homeland = cu:marble_dwarf
		}
	}	
}