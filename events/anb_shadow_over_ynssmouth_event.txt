﻿namespace = shadow_over_ynnsmouth

shadow_over_ynnsmouth.1 = {
	type = country_event
	placement = root
	
	title = shadow_over_ynnsmouth.1.t
	desc =  shadow_over_ynnsmouth.1.d
	flavor = shadow_over_ynnsmouth.1.f
	
	event_image = {
		video = "europenorthamerica_russian_serfs"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_map.dds"
	
	duration = 0

	hidden = no #for some reason it needs to be visible otherwise it does  not work
	
	trigger = {
	}
	

	immediate = {
		setup_racial_flags = yes
	}

	option = { 
		name = shadow_over_ynssmouth.a
		default_option = yes
		every_country = {
			setup_racial_flags = yes
		}
		setup_racial_flags = yes
	}	
}

